using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       // gameObject.transform.position = new Vector2(1, 0) * speed * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<PlayerMovement>())
        {
            var damaged = collision.gameObject.GetComponent<PlayerHealth>();
            damaged.DeductHealth(10);
            Destroy(gameObject);
        }
    }
}
