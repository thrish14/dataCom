using Mirror;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEngine;

public class MyNetworkPlayer : NetworkBehaviour
{
    
    [SerializeField] private TMP_Text displayNameText = null;
    [SerializeField] private Renderer displayColorRenderer = null;

    [SyncVar(hook = nameof(HandleDisplayNameUpdate))]
    [SerializeField]
    private string displayName = "Missing Name";
    //private Color displayNameColor = ;

    [SyncVar(hook = nameof(HandleDisplayColourUpdate))]
    [SerializeField]
    private Color displayColor = Color.black;

    #region server
    [Server]
    public void setDisplayName(string newDisplayName/*Color displayNameColor*/)
    {

        displayName = newDisplayName;
        //displayNameColor

    }

    [Server]
    public void setDisplayColor(Color newDisplayColor)
    {

        displayColor = newDisplayColor;

    }


    [Command]
    public void CmdSetDisplayName(string newDisplayName)
    {

        //server authority to limit displayName into 2 - 20 letter length
        if(newDisplayName.Length < 2 || newDisplayName.Length > 20)
        {
            RpcDisplayNewName(newDisplayName);
            setDisplayName(newDisplayName);
        }

    }

    #endregion

    #region client

    private void HandleDisplayColourUpdate(Color oldColor, Color newColor)
    {

        //displayColorRenderer.material.SetColor("_BaseColor", newColor);
        displayColorRenderer.material.color = newColor;

    }

    private void HandleDisplayNameUpdate(string oldName, string newName)
    {

        displayNameText.text = newName;

    }

    [ContextMenu("Set This Name")]
    private void setThisName()
    {

        CmdSetDisplayName("My New Name");

    }

    [ClientRpc]
    private void RpcDisplayNewName(string newDisplayName)
    {

        Debug.Log(newDisplayName);

    }

    #endregion

}
