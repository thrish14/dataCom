using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawn : MonoBehaviour
{
    public Transform spawner;
    public GameObject bulletPrefab;
    public void spawnBullet()
    {
        Instantiate(bulletPrefab, gameObject.transform);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
