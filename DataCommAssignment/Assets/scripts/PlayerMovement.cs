using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : NetworkBehaviour
{

    [SerializeField] private NavMeshAgent agent = null;

    private Camera mainCamera;

    public BulletSpawn spawn;
    #region server
    [Command]

    private void CmdMove(Vector3 pos)
    {

        if(!NavMesh.SamplePosition(pos, out NavMeshHit hit, 1f, NavMesh.AllAreas))
        {

            return;

        }

        agent.SetDestination(hit.position);

    }

    private void bullet()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //NetworkServer.Spawn();
            spawn.spawnBullet();
            Debug.Log("hi");
        }
    }

    #endregion

    #region client

    //start method for the client who owns the object
    public override void OnStartAuthority()
    {

        mainCamera = Camera.main;

    }

    [ClientCallback] //make it a client entity update all clients

    private void Update()
    {
        
        if(!isOwned)
        {
            return;
        }

        //check the right mouse button input
        if(!Input.GetMouseButtonDown(0)) 
        {
            return;
        }

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        //check the scene where it is hit
        if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
        {
            return;
        }

        CmdMove(hit.point);
        if(!Input.GetKeyDown(KeyCode.Space) )
        {
            return;
        }
        bullet();

    }

    #endregion

}
