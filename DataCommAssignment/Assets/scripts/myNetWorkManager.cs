using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class myNetWorkManager : NetworkManager
{

    public override void OnClientConnect()
    {
        base.OnClientConnect();
        Debug.Log("Server connected");
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient kon)
    {
        base.OnServerAddPlayer(kon);
        MyNetworkPlayer player = kon.identity.GetComponent<MyNetworkPlayer>();

        player.setDisplayName($"Player{numPlayers}");

        Color displayColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

        player.setDisplayColor(displayColor);

        Debug.Log($"Number of players added: {numPlayers}");
    }

    

}
