using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float maxHealth;

    public void DeductHealth(float value)
    {
        maxHealth -= value;
    }

    // Update is called once per frame
    void Update()
    {
        if(maxHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
}
